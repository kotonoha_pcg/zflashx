//Dialog Test
//30 Oct 2018 14:50

import java.awt.*;
import javax.swing.*;

int dropSpeed = 0;

void setup() {
    size(100, 100);
}

//copy from sketch_181114a.pde
void draw() {
    Object backSpeedValue = requestSpeedPreset();
    if(backSpeedValue == "Slow") {
        dropSpeed = 1;
    } else if(backSpeedValue == "Middle") {
        dropSpeed = 3;
    } else if(backSpeedValue == "Fast") {
        dropSpeed = 7;
    } else {
        dropSpeed = 1;
    }

    println("dropSpeedの値は: " + dropSpeed);
    noLoop();
}

Object requestSpeedPreset() {
    Object[] pstList = {"Slow", "Middle", "Fast"};
    //create dialog box
     Object speedPreset = JOptionPane.showInputDialog(
         null,
         "Choose One", "Input",
         JOptionPane.INFORMATION_MESSAGE, null,
         pstList, pstList[0]
     );
     return speedPreset;
}
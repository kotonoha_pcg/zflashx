//DialogAPI.pde
//coder::kotonohapcg

import java.awt.*;
import javax.swing.*;

class Settingdialog {
    String dMsg;
    int parseInValue;

    //値を設定するダイアログを作成する
    int createDialog(String dMsg) {
      String inputValue = JOptionPane.showInputDialog(dMsg);

      //Stringで出てきた値をintにパースする処理を追加
      try {
        parseInValue = Integer.parseInt(inputValue);
        println("Intに変換した値は" + parseInValue + "。");
      } catch (NumberFormatException e) {
        println("例外:" + e + ", not number format");
      } finally {
          println(dMsg + parseInValue + "で設定されました");
      }

      return parseInValue;
    }

    Object requestSpeedPreset() {
        Object[] pstList = {"Slow", "Middle", "Fast"};
        //create dialog box
         Object speedPreset = JOptionPane.showInputDialog(
             null,
             "ハイスピードの値を選択してください", "ハイスピード設定",
             JOptionPane.INFORMATION_MESSAGE, null,
             pstList, pstList[0]
         );
         return speedPreset;
    }

    int setHsValue(Object sp) {
      int ds = 0;
      if(sp == "Slow") {
          ds = 2;
      } else if(sp == "Middle") {
          ds = 5;
      } else if(sp == "Fast") {
          ds = 8;
      } else {
          ds = 1;
      }
      println("dropSpeedの値は: " + ds);
      return ds;
    }
}
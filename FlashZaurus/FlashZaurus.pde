//zFlashX
//coder::kotonohapcg

//add dialog box
import java.awt.*;
import javax.swing.*;

//定数定義
static int judgeZone = 20;

//変数定義
int[][] barPos = new int[judgeZone][2];
//ハイスピード調整
int hiSpeed = 50;
//hs as hispeed
int hs = 0;
//y-axis move distance
int ymd = 50;
int pointY = 0;
float barMed = 0;

//判定領域の数を設定
int splitArea = 0;
String gmMsg = "";
boolean game_play_state = false;
int msec = 0;

String infoMsg = "";

int dropSpeed = 0;

Settingdialog sdialog = new Settingdialog();


void setup() {
  size(1000, 1000);
  frameRate(120);

  //長方形を描く方向に変更
  rectMode(CENTER);
  
  colorMode(RGB, 256);
  background(128);

  //ハイスピードを設定する
  dropSpeed = sdialog.setHsValue(sdialog.requestSpeedPreset());
  //判定領域
  splitArea = 10;

  rebuild();
  arrReset();

}

//配列値の初期化
void arrReset() {
  //barPos[][0]の値を初期化する
  for(int i = 0; i < splitArea; i++) {
    for(int j = 0; j < 2; j++) {
        barPos[i][j] = 0;
    }
  }
}

//画面の描画初期化
void rebuild() {
  background(255, 250, 250);
  //枠線を引く
  for (int i = 0; i <= 1000; i += (1000 / splitArea)) {
    line(200, i, 800, i);
    line(200, i, 200, i + 100);
    line(800, i, 800, i + 100);
  }
}


void draw() {
  if(keyPressed == true && key == ' ') {
    game_play_state = true;
    drawRect(dropSpeed);
  }
}

void drawRect(int ds) {
  //dsはあくまでも移動量なので、y座標の移動量に影響を与えてはいけない
    rebuild();
    fill(50);
    rect(500, ymd, 200, 25);
    ymd += ds;

    //判定処理関係
    //判定バーの下端を格納する方式
    barPos[ymd / (1000 / splitArea)][0] = 1;
    //判定バーの上端を格納する方式
    barPos[(ymd - 25) / (1000 / splitArea)][1] = 1;
    
    //判定バー中央のy座標
    barMed = ymd - ((25/2) + (25%2));
    if(ymd >= 1000) {
      gmMsg = "エリア超過！";
      gameOver(gmMsg);
    }
}

//判定処理
void keyReleased() {
  if(key == ' ') {
    println("Space key is released");
    //show info
    println("上端座標:" + ymd + " / 下端座標:" + (ymd - 25));
    println("判定バーの中央座標: " + barMed);

    //判定ラインをbarPos[8][]にした場合
    if(barPos[8][0] == 1 && barPos[8][1] == 1) {
      if(barPos[9][0] == 0) {
        if(barMed == (ymd/100) * 100 + 50) {
          gmMsg = "Excellent!";
        } else {
          gmMsg = "JUST!";
        }
      } else {
        gmMsg = "BAD";
      }
    } else {
        gmMsg = "MISS";
    }

    gameOver(gmMsg);
  }

}

//位置のずれ判定
void barGap(float barMed, int ymd) {
  int posGap = (int)barMed - (ymd/100) * 100 + 50;
  println((ymd/100) * 100 + 50);
  println("あと" + posGap + "でExcellentです！");
}

//ゲームオーバー時処理
void gameOver(String gmMsg) {
  println(gmMsg);
  textSize(60);
  fill(220, 20, 60);
  text(gmMsg, 20, 100);
  game_play_state = false;
  noLoop();
}
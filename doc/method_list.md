# Method List

## FlashZaurus.pde

### void rebuild()
- 背景、領域の枠線をsplitAreaの値に応じて描画する

メソッド名から推測できるように、判定バーを移動させる都度、呼び出される

### void draw()
- Loopである限りその中身を実行する
    - noLoop()指定では都度呼び出す

### void drawRect(int ds)
- 長方形を描画する
- ds分だけrectのy座標移動量を増加させる

判定バーが領域内に入っているか確認する方法として、それぞれ上端・下端をbarPos[][]に格納する方式を取った。

また、y座標が1000以上になったときにgameOverへgmMsgを渡す。

### keyReleased()
システムで用意されているメソッド。ここでは、スペースキーを離したときに実行される。

- barPos[8][]を判定ラインに仮設定した際に、下端(barPos[8][0])及び上端(barPos[8][1])が'1'なら、成功
    - この条件に合わなければ失敗

### gameOver(String gmMsg)
- drawRect()内で領域外に図形が出ていった場合の処理を記述する
- もしくはgmMsgに指定された文字列を表示して判定評価を左上に表示する

---
## DialogAPI.pde

### int createDialog()
- 汎用的に使えるダイアログ
- ゲーム実行時に一度だけ呼びだされ、判定領域の数を整数型で入力する(推奨:10)

例外処理として、入力値を整数型へパースする際、String型などが入力されていた場合は、
再度ダイアログを呼びだす。

### requestSpeedPreset()
- ハイスピードの値を選択するダイアログ作成メソッド
- ゲーム実行時に一度だけ呼びだされ、ハイスピードを"Slow", "Middle", "Fast"の3つから選ぶ

### setHsValue()
- requestSpeedPreset()の戻り値を元に、ハイスピードの値を整数型に置き換える
- 落下速度dsを返却する
---
## DialogAPI

### int createDialog()

### Object requestSpeedPreset()
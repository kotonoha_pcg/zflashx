# varlist

## about sketch_181114a

### barPos[][]
- (T) array[x][y]
- 画像barの表示位置と連動する
    - 描画された領域内と配列内との位置が等しいかどうかで判定する
- **lengthが20なのは、現状20で区切っているため。** この領域数は、開始前んうぃダイアログで指定可能。

### judgeArea[][]
- (T) array[x][y]
- [x]は描画される領域数を示す
- [y]は0では各領域の始点座標を示し、1では各領域の中心座標を示す

### game_play_state
- trueの時ゲームが進行する
    - falseの時はゲームが進行していない
- defaultでは`false`である


### ymd
- Y axis Move Distance

y座標の移動距離を格納する。また、座標から判定バーの判定を行う。

### barMed
判定バーの中央のy座標を格納する。ymdと判定領域の中央(1-100なら50)座標が一致すると、

keyReleased()でgmMsgに`Excellent`と代入する。